<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

//v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        $api->post('employee-register-shift/create',[
            'uses'=>'EmployeeRegisterShiftController@registerShift'
        ]);
        $api->post('employee-register-shift/delete',[
            'uses'=>'EmployeeRegisterShiftController@deleteRegisterShift'
        ]);
        $api->get('employee-register-shift/get-list',[
            'uses'=>'EmployeeRegisterShiftController@list'
        ]);
        $api->post('employee-register-shift/enroll-shift',[
            'uses'=>'EmployeeRegisterShiftController@makeEnrollShiftToEmp'
        ]);
    });
});
