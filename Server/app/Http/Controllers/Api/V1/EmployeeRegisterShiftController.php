<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Branch;
use App\Api\Entities\EmployeeRegisterShift;
use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use App\Api\Entities\WorkingSheet;
use App\Api\Repositories\Contracts\EmployeeRegisterShiftRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\APIs\APIEmployee;

class EmployeeRegisterShiftController extends Controller{
    protected $request;
    protected $employeeRegisterShiftRepository;

    public function __construct(Request $request,EmployeeRegisterShiftRepository $employeeRegisterShiftRepository)
    {
        $this->request = $request;
        $this->employeeRegisterShiftRepository = $employeeRegisterShiftRepository;
    }

    public function registerShift(){
        $validator = \Validator::make($this->request->all(),[
            'emp_id'=>'required',
            'org_id'=>'required',
            'branch_id'=>'required',
            'shift_selected'=>'required',
            'date'=>'date|required',
            'note'=>'string|max:255'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $emp_id = $this->request->get('emp_id');
        $org_id = $this->request->get('org_id');
        $shift_selected = $this->request->get('shift_selected');
        $note = $this->request->get('note');
        $branch_id = $this->request->get('branch_id');
        $date = $this->request->get('date');

        //kiểm tra thông tin nhân viên đăng kí ca
        $emp_info = UserEmployee::where(['_id'=>mongo_id($emp_id)])->first();
        if(empty($emp_info)){
            return $this->errorBadRequest(trans('employee.no_employee'));
        }

        //Kiểm tra thông tin tổ chức
        $org_info = Organizations::where(['_id'=>mongo_id($org_id)])->first();
        if(empty($org_info)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }

        //Kiểm tra thông tin chi nhánh
        $branch_info = Branch::where([
            '_id' => mongo_id($branch_id)
        ])->first();
        if(empty($branch_info)){
            return $this->errorBadRequest(trans('branch.no-branch'));
        }

        //Kiểm tra thông tin ca làm
        $shift_info = WorkingSheet::where(['_id'=>mongo_id($shift_selected)])->first();
        if(empty($shift_info)){
            return $this->errorBadRequest(trans('workingSheet.no_working_shift'));
        }

        $attributes = [
            'emp_id'=>$emp_id,
            'org_id'=>$org_id,
            'branch_id' => $branch_id,
            'shift_selected'=>$shift_selected,
            'date' => $date
        ];

        $registerShift = EmployeeRegisterShift::create($attributes);
        if(!empty($note)){
            $registerShift['note'] = $note;
        }
        $registerShift->save();

        return $this->successRequest(trans('core.success'));
    }

    public function deleteRegisterShift(){
        $validator = \Validator::make($this->request->all(),[
            'shift_id'=>'required',
            'deleted_user'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $shift_id = $this->request->get('shift_id');//djson($shift_id);
        $deleted_user = $this->request->get('deleted_user');

        //Kiểm tra thông tin người xoá đăng ký ca
        $emp_info =  UserEmployee::where(['_id'=>mongo_id($deleted_user)])->first();
        if(empty($emp_info)){
            return $this->errorBadRequest(trans('employee.no_employee'));
        }

        //Kiểm tra thông tin ca làm
        $shift_info = EmployeeRegisterShift::where(['_id'=>mongo_id($shift_id)])->first();
        if(empty($shift_info)){
            return $this->errorBadRequest(trans('workingSheet.no_working_shift'));
        }

        $shift_info->deleted_user = $deleted_user;
        $shift_info->save();
        $shift_info->delete();

        return $this->successRequest(trans('core.success'));
    }

    public function list(){
        $validator = \Validator::make($this->request->all(),[
            'org_id'=>'required',
            'branch_id' => 'nullable',
            'emp_id'=>'nullable',
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $org_id = $this->request->get('org_id');
        $emp_id = $this->request->get('emp_id');
        $branch_id = $this->request->get('branch_id');

        //kiểm tra thông tin tổ chức
        $org_info = Organizations::where(['_id'=>mongo_id($org_id)])->first();
        if(empty($org_info)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }


        $params = [
            'org_id'=>$org_id,
            'branch_id' => $branch_id,
            'emp_id'=>$emp_id
        ];

        $getRegisterShift = $this->employeeRegisterShiftRepository->getRegisterShift($params);
        $data = [];

        foreach($getRegisterShift as $item){
            $data[] = $item->transform();
        }

        return $this->successRequest($data);
    }

    public function makeEnrollShiftToEmp(){
        $validator = \Validator::make($this->request->all(),[
            '_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('_id');

        //kiểm tra thông tin ca đã đăng kí
        $registeredShift = EmployeeRegisterShift::where([
            '_id'=>mongo_id($id)
        ])->first();
        if(empty($registeredShift)){
            return $this->errorBadRequest(trans('employee.no_registered_shift'));
        }

        //Tạo công cho ca làm
        $params = [
            'shift_id' => $id,
            'branch_id' => $registeredShift->branch_id,
            'org_id' => $registeredShift->org_id,
            'emp_id' => $registeredShift->emp_id,
            'date' => $registeredShift->date
        ];
        $enrollShift = APIEmployee::enrollShift($params);

        $registeredShift->status = 1;
        $registeredShift->save();
        return $this->successRequest(trans('employee.confirmed'));
    }
}