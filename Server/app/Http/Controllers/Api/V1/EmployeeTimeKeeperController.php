<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Branch;
use App\Api\Entities\EmployeeTimeKeeper;
use App\Api\Entities\UserEmployee;
use App\Api\Repositories\Contracts\EmployeeTimeKeeperRepository;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\APIs\APIEmployee;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EmployeeTimeKeeperController extends Controller{
    protected $request;
    protected $employeeTimeKeeperRepository;

    public function __construct(Request $request,EmployeeTimeKeeperRepository $employeeTimeKeeperRepository)
    {
        $this->request = $request;
        $this->employeeTimeKeeperRepository = $employeeTimeKeeperRepository;
    }

    //chấm công vào ca ra ca
    public function checkInOut(){
        $validator = \Validator::make($this->request->all(),[
            'shift_id'=>'required',
            'type' => 'required',
            'time'=>'required|date_format:H:i'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
        $shift_id = $this->request->get('shift_id');
        $type = $this->request->get('type');
        $time = $this->request->get('time');

        $checkShift = EmployeeTimeKeeper::where([
            '_id' => mongo_id($shift_id)
        ])->first();

        if(empty($checkShift)){
            return $this->errorBadRequest(trans('employeeTimeKeeper.no-shift'));
        }

        $params = [
            'shift_id' => $shift_id,
            'type' => $type,
            'time' => $time
        ];

        $checkTime = APIEmployee::checkInOut($params);
        return $this->successRequest(trans('core.success'));
    }

    //Danh sách lịch sử chấm công theo option
    public function listShiftEnrolled(){
        $validator = \Validator::make($this->request->all(),[
            'branch_id'=>'required',
            'option'=>[
                Rule::in([0,1,2]),
                'required'
            ],
            'from_date'=>'date',
            'to_date'=>'date|after:from_date'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $branch_id = $this->request->get('branch_id');
        $option = $this->request->get('option');
        $emp_id = $this->request->get('emp_id');
        $from_date = $this->request->get('from_date');
        $to_date = $this->request->get('to_date');

        //Kiểm tra thông tin chi nhánh
        $branch_info = Branch::where([
            '_id'=>mongo_id($branch_id)
        ])->first();
        if(empty($branch_info)){
            return $this->errorBadRequest(trans('branch.no-branch'));
        }

        //Kiểm tra thông tin nhân viên
        if(isset($emp_id)){
            $emp_info = UserEmployee::where([
                '_id'=>mongo_id($emp_id)
            ])->first();
            if(empty($emp_info)){
                return $this->errorBadRequest(trans('employee.no_employee'));
            }
        }

        $params = [
            'branch_id' => $branch_id,
            'emp_id' => $emp_id,
            'from_date' => $from_date,
            'to_date' => $to_date
        ];
        $data = [];

        /**
         * - Nếu option = 0, xuất ra tất cả phiếu công của chi nhánh hoặc nhân viên chi nhánh đó
         * - Nếu option = 1, xuất ra danh sách các phiếu công chưa được duyệt (status = 0)
         * - Nếu option = 2, xuất ra danh sách các phiếu công đã được chuyệt (status != 0)
         */
        if($option == 1){
            $params['status'] = -1;
            $notConfirmedShift = $this->employeeTimeKeeperRepository->getTimeKeeper($params);

            foreach($notConfirmedShift as $item){
                $data[] = $item->transform();
            }
        }elseif($option == 2){
            $params['status'] = 1;
            $confirmedShift = $this->employeeTimeKeeperRepository->getTimeKeeper($params);

            foreach($confirmedShift as $item){
                $data[] = $item->transform();
            }
        }else{
            $listShift = $this->employeeTimeKeeperRepository->getTimeKeeper($params);
            foreach($listShift as $item){
                $data[] = $item->transform();
            }
        }
        
        return $this->successRequest($data);
    }
}