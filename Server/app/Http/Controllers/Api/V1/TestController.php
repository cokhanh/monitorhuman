<?php
namespace App\Http\Controllers\Api\V1;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\Entities\Test;
use App\Libraries\Gma\APIs\APIUpload;
use App\Libraries\Gma\APIs\APIWifi;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class TestController extends Controller {
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    //prevent app from sleeping
    public function test(){
        return $this->successRequest('From KKH Team with love');
    }
    //get client ip
    public function testWifi(){
        djson($this->request->ip());
    }

    public function store(){
        
        $param = [
            'org_id'=>'5f36052e2c1d0000f10071e2',
            'branch_id'=>'VVT',
            'user_id'=>'DCK',
            'type'=>'image',
            'option'=>'avatars'
        ];
        APIUpload::uploadToServer($param,$this->request);
        // $data = $this->request->input('image');
        // $photo = $this->request->file('image')->getClientOriginalName();
        // $destination = base_path() . '/public/img';
        // $temp = $this->request->file('image')->move($destination, $photo);

        dd('sds');
    }
    //gọi API trả về đường link lấy ảnh
    public function getImage(){
        //dd('sd');
        $params = [
            'type'=>'image',
            'org_id'=>'5f36052e2c1d0000f10071e2',
            'branch_id'=>'VVT3',
            'user_id'=>'DCK3',
            'option'=>'avatars'
        ];
        //trả về full path
        $temp = APIUpload::getFileToClient($params);
        return $this->successRequest($temp);
        //tách full path ra thành 2 phần riêng để tránh trường hợp bị load đi load lại một hình
        if($params['option'] == 'avatar'){
            $name = substr(strrchr($temp,"avatars/"),4);
            //djson($temp);
            $path = substr(strrchr($temp,"/"),1);
            djson($path);
        }

        $filename = basename($temp);
        $file_extension = 'image/'.strtolower(substr(strrchr($filename,"."),1));

        if(validate_image_extention($file_extension)){
            $headers = ['Content-Type' => $file_extension];
            $path = '..'.$path . $name;
            djson($path);
            $response = new BinaryFileResponse($path,200,$headers);

            return $response;
        }
        return $this->errorBadRequest('core.invalid_format');
    }

    public function loadImage(){
        $validator = \Validator::make($this->request->all(),[
            'name'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest(trans('core.invalid_format'));
        }
        $img_name = $this->request->get('name');

        $filename = basename($img_name);
        $file_extension = 'image/'.strtolower(substr(strrchr($filename,"."),1));

        if(validate_image_extention($file_extension)){
            $headers = ['Content-Type' => $file_extension];
            $path = '../public/img/avatars/' . $filename;
            //kiểm tra có path trong source
            if(file_exists($path)){
                $response = new BinaryFileResponse($path,200,$headers);
                return $response;
            }

            $path = '../public/img/avatars/default.jpg';
            $response = new BinaryFileResponse($path,200,$headers);
            return $response;
        }
        return $this->errorBadRequest(trans('core.invalid_format'));
    }
}