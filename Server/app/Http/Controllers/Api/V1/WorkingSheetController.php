<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use App\Api\Entities\WorkingSheet;
use App\Api\Repositories\Contracts\WorkingSheetRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WorkingSheetController extends Controller{
    protected $request;
    protected $workingSheetRepository;

    public function __construct(Request $request,WorkingSheetRepository $workingSheetRepository)
    {
        $this->request = $request;
        $this->workingSheetRepository = $workingSheetRepository;
    }

    public function create(){
        $validator = \Validator::make($this->request->all(),[
            'shift_name'=>'string|required',
            'start'=>'required|date_format:H:i',
            'end'=>'required|date_format:H:i|after:start',
            'org_id'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $shift_name = $this->request->get('shift_name');
        $start = $this->request->get('start');
        $end = $this->request->get('end');
        $org_id = $this->request->get('org_id');

        $checkORG = Organizations::where(['_id'=>mongo_id($org_id)])->first();
        if(empty($checkORG)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }

        $attrs = [
            'shift_name'=>$shift_name,
            'no_sign'=>build_slug($shift_name),
            'start'=>$start,
            'end'=>$end,
            'org_id'=>$org_id
        ];

        $workingSheet = WorkingSheet::create($attrs);
        return $this->successRequest(trans('core.success'));
    }
    public function update(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'string|required',
            'start'=>'nullable|date_format:H:i',
            'end'=>'nullable|date_format:H:i',
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $start = $this->request->get('start');
        $end = $this->request->get('end');

        $params = [
            'is_detail'=>1,
            'id'=>$id
        ];

        $checkWorkShift = $this->workingSheetRepository->getWorkingSheetShift($params);
        if(empty($checkWorkShift)){
            return $this->errorBadRequest(trans('workingSheet.no_working_shift'));
        }

        if(isset($start)){
            $checkWorkShift->start = $start;
        }
        if(isset($end)){
            $checkWorkShift->end = $end;
        }

        $checkWorkShift->save();
        return $this->successRequest(trans('core.success'));
    }
    public function delete(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'required',
            'user_id'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $user = $this->request->get('user_id');

        $params = [
            'id'=>$id,
            'is_detail'=>1
        ];

        $checkWorkShift = $this->workingSheetRepository->getWorkingSheetShift($params);
        if(empty($checkWorkShift)){
            return $this->errorBadRequest(trans('workingSheet.no_working_shift'));
        }

        //kiểm tra thông tin user
        $userInfo = UserEmployee::where(['_id'=>mongo_id($user)])->first();
        if($userInfo->org_id != $checkWorkShift->org_id){
            return $this->errorBadRequest(trans('employee.is-not-staff-of-org'));
        }

        if(empty($userInfo)){
            return $this->errorBadRequest(trans('employee.no_employee'));
        }
        $checkWorkShift->deleted_user = $user;
        $checkWorkShift->save();
        $checkWorkShift->delete();

        return $this->successRequest(trans('core.success'));
    }
    public function list(){
        $validator = \Validator::make($this->request->all(),[
            'org_id'=>'required',
            'start'=>'nullable',
            'end'=>'nullable',
            'option'=>'nullable'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $org_id = $this->request->get('org_id');
        $start = $this->request->get('start');
        $end = $this->request->get('end');
        $option = $this->request->get('option');

        $params = [
            'org'=>$org_id,
            'is_paginate'=>$this->request->get('is_paginate'),
            'start'=>$start,
            'end'=>$end
        ];

        $workShiftByOrg = $this->workingSheetRepository->getWorkingSheetShift($params);
        //djson($workShiftByOrg);
        $data = [];
        foreach($workShiftByOrg as $item){
            $data[] = $item->transform();
        }

        return $this->successRequest($data);
    }
}