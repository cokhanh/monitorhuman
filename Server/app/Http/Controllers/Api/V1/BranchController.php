<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Organizations;
use App\Http\Controllers\Controller;
use App\Api\Entities\Branch;
use App\Api\Repositories\Contracts\BranchRepository;
use Illuminate\Http\Request;

class BranchController extends Controller{
    protected $request;
    protected $branchRepository;

    public function __construct(Request $request,BranchRepository $branchRepository)
    {
        $this->request = $request;
        $this->branchRepository = $branchRepository;
    }

    public function create(){
        $validator = \Validator::make($this->request->all(),[
            'branch_name'=>'required|unique:Branch',
            'org_id'=>'required',
            'phone'=>'required|min:10|max:11|unique:Branch',
            'address'=>'string|required',
            'email'=>'email|required|unique:Branch',
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $branch_name = $this->request->get('branch_name');
        $org_id = $this->request->get('org_id');
        $phone = $this->request->get('phone');
        $email = $this->request->get('email');
        $address = $this->request->get('address');

        $checkORG = Organizations::where(['_id'=>mongo_id($org_id)])->first();
        if(empty($checkORG)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }

        $attrs = [
            'branch_name'=>$branch_name,
            'org_id'=>$org_id,
            'no_sign'=>build_slug($branch_name),
            'phone'=>$phone,
            'email'=>$email,
            'address'=>$address
        ];

        $branch = Branch::create($attrs);
        return $this->successRequest(trans('core.success'));
    }

    public function update(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'required',
            'branch_name'=>'unique:Branch',
            'email'=>'email|unique:Branch',
            'phone'=>'unique:Branch'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $branch_name = $this->request->get('branch_name');
        $email = $this->request->get('email');
        $phone = $this->request->get('phone');

        $params = [
            'is_detail'=>1,
            'id'=>$id
        ];

        $checkBranch = $this->branchRepository->getBranch($params);
        if(empty($checkBranch)){
            return $this->errorBadRequest(trans('branch.no-branch'));
        }
        if(isset($branch_name)){
            $checkBranch->branch_name = $branch_name;
            $checkBranch->no_sign = build_slug($branch_name);
        }
        if(isset($email)){
            $checkBranch->email = $email;
        }
        if(isset($phone)){
            $checkBranch->phone = $phone;
        }

        $checkBranch->save();
        return $this->successRequest(trans('core.success'));
    }

    public function delete(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $params = [
            'id'=>$id,
            'is_detail'=>1
        ];

        $checkBranch = $this->branchRepository->getBranch($params);
        if(empty($checkBranch)){
            return $this->errorBadRequest(trans('branch.no-branch'));
        }

        $checkBranch->deleted_user = $this->request->get('deleted_user');
        $checkBranch->save();
        $checkBranch->delete();
        return $this->successRequest(trans('core.success'));
    }

    public function list(){
        $validator = \Validator::make($this->request->all(),[
            'org_id'=>'required',
            'branch_name'=>'nullable',
            'phone'=>'nullable'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $org_id = $this->request->get('org_id');
        $branch_name = $this->request->get('branch_name');
        $phone = $this->request->get('phone');
        $is_paginate = $this->request->get('is_paginate');
        $limit = $this->request->get('limit');

        $params = [
            'is_paginate'=>$is_paginate,
            'org_id'=>$org_id,
            'no_sign'=>build_slug($branch_name),
            'phone'=>$phone
        ];

        $getBranch = $this->branchRepository->getBranch($params,$limit);
        $data = [];
        foreach($getBranch as $item){
            $data[] = $item->transform();
        }

        return $this->successRequest($data);
    }
}
