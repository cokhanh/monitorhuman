<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\UserEmployee;
use Illuminate\Http\Request;
use App\Api\Entities\Users;
use App\Api\Entities\UserToken;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthManager;

class UserController extends Controller{
    protected $request;
    protected $usersRepository;
    protected $auth;

    public function __construct(Request $request,UsersRepository $usersRepository,AuthManager $auth)
    {
        $this->request = $request;
        $this->usersRepository = $usersRepository;
        $this->auth = $auth;

        parent::__construct();
    }

    public function register(){
        $user_name = 'cokhanh';
        $password = '1';

        $attr = [
            'user_name'=>$user_name,
            'password'=>$password
        ];
        $user = Users::create($attr);
        return $this->successRequest(trans('core.success'));
    }

    public function logIn(){
        $validator = \Validator::make($this->request->all(),[
            'user_name'=>'required',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $user_name = $this->request->get('user_name');
        $password = $this->request->get('password');

        //check user name có tồn tại
        $params = [
            'user_name'=>$user_name
        ];

        $user_list = $this->usersRepository->getUsersList($params);
        
        if(empty($user_list)){
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $is_valid_account = 0; //0: Là không có tài khoản, 1: Có tài khoản
        foreach($user_list as $item){
            if(password_verify($password,$item->password)){
                $is_valid_account = 1;
                $user_record = $item;
            }
        }

        //Nếu User có tồn tại tại thì add user vào token.
        if($is_valid_account == 1){
            //Lấy các thông tin của user đăng nhập
            $emp_info = UserEmployee::where([
                '_id'=>mongo_id($user_record->user_id)
            ])->first();
            if(empty($emp_info)){
                return $this->errorBadRequest(trans('employee.no_employee'));
            }

            $customClaims = [];
            $customClaims['user'] = $user_record->user_name;
            $customClaims['user_id'] = $user_record->user_id;
            $customClaims['org_id'] = $user_record->org_id;
            $customClaims['branch_id'] = $user_record->branch_id;
            $customClaims['user_full_name'] = $emp_info->full_name;
            $customClaims['role'] = $emp_info->role;

            $token = $this->auth->customClaims($customClaims)->fromUser($user_record);
            
            $attribute = [
                'user_id' => $user_record->user_id,
                'api_token' => $token,
            ];

            UserToken::create($attribute);
            $user_record->updated_at = Carbon::now();
            $user_record->save();
            $data = [
                'token' => $token,
                'user_id' => $this->request->get('user_name')
            ];
            return $this->successRequest($data);
        }
        
        return $this->errorBadRequest(trans('auth.incorrect'));
    }

    public function logOut(){
        $validator = \Validator::make($this->request->all(),[
            'api_token' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
        //client truyền token lên kèm với bearer (bearer + '' + token)
        $user = Auth::user();
        $userToken = UserToken::where([
            'user_id'=>$user->user_id,
            'api_token'=>$this->request->get('api_token')
        ])->first();
        if(!empty($userToken)){
            $userToken->delete();
            return $this->successRequest(trans('core.success'));
        }

        return $this->errorBadRequest([]);
    }
    public function validateLogin(){
        $validator = \Validator::make($this->request->all(), [
            'api_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $user = Auth::user();
        $userToken = UserToken::where(['user_id'=> $user->user_id, 'api_token' => $this->request->get('api_token')])->first();
        if(!empty($userToken)){
            return $this->successRequest(trans('core.success'));
        }
        return $this->errorBadRequest([]);
    }
}