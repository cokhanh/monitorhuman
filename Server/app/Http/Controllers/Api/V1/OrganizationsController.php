<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Organizations;
use App\Api\Repositories\Contracts\OrganizationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationsController extends Controller
{
    protected $request;
    protected $organizationsRepository;

    public function __construct(Request $request,OrganizationsRepository $organizationsRepository)
    {
        $this->request = $request;
        $this->organizationsRepository = $organizationsRepository;
    }

    public function list(){
        return $this->successRequest(Organizations::all());
    }
    public function create(){
        $validator = \Validator::make($this->request->all(),[
            'org_name'=>'string|required|unique:organizations',
            'phone'=>'string|required|unique:organizations',
            'email'=>'email|required|unique:organizations'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $org_name = $this->request->get('org_name');
        $phone = $this->request->get('phone');
        $email = $this->request->get('email');

        $attrs = [
            'org_name'=>$org_name,
            'phone'=>$phone,
            'email'=>$email
        ];

        $org = Organizations::create($attrs);
        return $this->successRequest(trans('core.success'));
    }

    public function update(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'string|required',
            'org_name'=>'string|unique:organizations,'.(string)($this->request->get('id')),
            'phone'=>'string|unique:organizations,'.(string)($this->request->get('id')),
            'email'=>'email|unique:organizations,'.(string)($this->request->get('id'))
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = mongo_id($this->request->get('id'));
        $org_name = $this->request->get('org_name');
        $phone = $this->request->get('phone');
        $email = $this->request->get('email');

        $checkORG = Organizations::where(['_id'=>$id])->first();
        if(empty($checkORG)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }

        if(isset($org_name) && ($org_name != $checkORG->org_name)){
            $checkORG->org_name = $org_name;
        }
        if(isset($phone) && ($phone != $checkORG->phone)){
            $checkORG->phone = $phone;
        }
        if(isset($email) && ($email != $checkORG->email)){
            $checkORG->email = $email;
        }

        $checkORG->save();
        return $this->successRequest(trans('core.success'));
    }

    public function delete(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'string|required',
            'deleted_user'=>'string|required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = mongo_id($this->request->get('id'));
        $deleted_user = $this->request->get('deleted_user');

        $checkORG = Organizations::where(['_id'=>$id])->first();
        if(empty($checkORG)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }

        //check user
        //end check user
        $checkORG->deleted_user = $deleted_user;
        $checkORG->save();
        $checkORG->delete();

        return $this->successRequest(trans('core.success'));
    }
}