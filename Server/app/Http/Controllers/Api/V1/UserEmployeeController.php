<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Branch;
use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use App\Api\Repositories\Contracts\UserEmployeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\APIs\APIAuth;
use Dingo\Api\Facade\API;
use Illuminate\Validation\Rule;

class UserEmployeeController extends Controller
{
    protected $request;
    protected $userEmployeeRepository;

    public function __construct(Request $request,UserEmployeeRepository $userEmployeeRepository)
    {
        $this->request = $request;
        $this->userEmployeeRepository = $userEmployeeRepository;
    }

    public function create(){
        $validator = \Validator::make($this->request->all(),[
            'full_name'=>'string|required',
            'birthday'=>'required|date',
            'phone'=>'string|required|min:10|max:11',
            'email'=>'email',
            'org_id'=>'string|required',
            'branch_id'=>'string|required',
            'sex'=>[
                'required',
                Rule::in([0,1])
            ],
            'option' => [
                'required',
                Rule::in([0,1])
                /**
                 * 0: Dùng thử.
                 * 1: Bản quyền.
                 */
            ],
            'role'=>[
                'required',
                Rule::in([0,1,2,3])
                /**
                 * 0: Quyền admin hệ thống.
                 * 1: Quyền Manager.
                 * 2: Quyền Branch.
                 * 3: Quyền Staff.
                 */
            ],
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $full_name = $this->request->get('full_name');
        $birthday = $this->request->get('birthday');
        $phone = $this->request->get('phone');
        $email = $this->request->get('email');
        $org = $this->request->get('org_id');
        $branch = $this->request->get('branch_id');
        $sex = $this->request->get('sex');
        $role = $this->request->get('role');
        $option = $this->request->get('option');

        //Xử lý role
        if($role == 0){
            $role = UserEmployee::SYSTEM_ADMIN;
        }elseif($role == 1){
            if($option == 0){
                $role = UserEmployee::MANAGER_TRIAL;
            }else{
                $role = UserEmployee::MANAGER;
            }
        }elseif($role == 2){
            if($option == 0){
                $role = UserEmployee::BRANCH_MANAGER_TRIAL;
            }else{
                $role = UserEmployee::BRANCH_MANAGER;
            }
        }else{
            if($option == 0){
                $role = UserEmployee::STAFF_TRIAL;
            }else{
                $role = UserEmployee::STAFF;
            }
        }

        $checkORG = Organizations::where(['_id'=>mongo_id($org)])->first();
        $checkBRANCH = Branch::where(['_id'=>mongo_id($branch)])->first();

        if(empty($checkORG)){
            return $this->errorBadRequest(trans('organizations.no_org'));
        }
        if(empty($checkBRANCH)){
            return $this->errorBadRequest(trans('branch.no-branch'));
        }
        
        $attr = [
            'full_name'=>$full_name,
            'no_sign'=>build_slug($full_name),
            'birthday'=>$birthday,
            'sex'=>$sex,
            'phone'=>$phone,
            'email'=>$email,
            'org_id'=>$org,
            'branch_id'=>$branch,
            'role'=>$role,
            'role_no_sign'=>build_slug($role)
        ];

        $employee = UserEmployee::create($attr);

        //Tạo tài khoản người dùng
        $params = [
            'user_id'=>$employee->_id,
            'user_name'=>$employee->full_name,
            'password'=>$employee->phone,
            'branch_id'=>$employee->branch_id,
            'org_id'=>$employee->org_id
        ];
        $createAccount = APIAuth::createUserAccount($params);
        return $this->successRequest(trans('core.success'));
    }

    public function update(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'string|required',
            'phone'=>'max:11|min:10',
            'email'=>'email',
            'sex'=>Rule::in(0,1),
            'role'=>[
                'required',
                Rule::in([0,1,2,3])
                /**
                 * 0: Quyền admin hệ thống.
                 * 1: Quyền Manager.
                 * 2: Quyền Branch.
                 * 3: Quyền Staff.
                 */
            ],
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $phone = $this->request->get('phone');
        $email = $this->request->get('email');
        $sex = $this->request->get('sex');
        $role = $this->request->get('role');

        //Xử lý role
        if($role == 0){
            $role = UserEmployee::SYSTEM_ADMIN;
        }elseif($role == 1){
            $role = UserEmployee::MANAGER;
        }elseif($role == 2){
            $role = UserEmployee::BRANCH_MANAGER;
        }else{
            $role = UserEmployee::STAFF;
        }

        $params = [
            'id'=>$id,
            'is_detail'=>1
        ];

        $checkEmp = $this->userEmployeeRepository->getEmployee($params);
        if(empty($checkEmp)){
            return $this->errorBadRequest(trans('employee.no_employee'));
        }

        if(isset($phone) && ($phone != $checkEmp->phone)){
            $checkEmp->phone = $phone;
        }
        if(isset($email) && ($email != $checkEmp->email)){
            $checkEmp->email = $email;
        }
        if(isset($sex) && ($sex != $checkEmp->sex)){
            $checkEmp->sex = $sex;
        }
        if(isset($role) && ($role != $checkEmp->role)){
            $checkEmp->role = $role;
            $checkEmp->role_no_sign = build_slug($role);
        }

        $checkEmp->save();
        return $this->successRequest(trans('core.success'));
    }

    public function delete(){
        $validator = \Validator::make($this->request->all(),[
            'id'=>'required',
            'deleted_user'=>'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id = $this->request->get('id');
        $deleted_user = $this->request->get('deleted_user');

        $params = [
            'id'=>$id,
            'is_detail'=>1
        ];

        $checkEmp = $this->userEmployeeRepository->getEmployee($params);
        if(empty($checkEmp)){
            return $this->errorBadRequest(trans('employee.no_employee'));
        }

        $checkEmp->deleted_user = $deleted_user;
        $checkEmp->save();
        $checkEmp->delete();

        return $this->successRequest(trans('core.success'));
    }

    public function list(){
        $validator = \Validator::make($this->request->all(),[
            'org_id'=>'required',
            'branch_id'=>'nullable',
            'sex'=>'nullable',
            'full_name'=>'nullable',
            'role'=>'nullable'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $org_id = $this->request->get('org_id');
        $branch_id = $this->request->get('branch_id');
        $sex = $this->request->get('sex');
        $no_sign = build_slug((string)$this->request->get('full_name'));
        $role_no_sign = build_slug((string)($this->request->get('role_no_sign')));

        $is_paginate = $this->request->get('is_paginate');
        $limit = $this->request->get('limit');

        $params = [
            'id'=>$this->request->get('id'),
            'is_paginate'=>$is_paginate,
            'org_id'=>$org_id,
            'branch_id'=>$branch_id,
            'sex'=>$sex,
            'no_sign'=>$no_sign,
            'role_no_sign'=>$role_no_sign
        ];

        $getEmp = $this->userEmployeeRepository->getEmployee($params,$limit);
        
        $data = [];
        foreach ($getEmp as $item){
            $data[] = $item->transform();
        }

        return $this->successRequest($data);
    }
}