<?php
namespace App\Libraries\Gma\APIs;

use App\Api\Entities\Upload;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class APIUpload {

    /**
     * Trả về request để gọi API lấy file
     * params truyền vào bao gồm:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - org_id: mã tổ chức
     * - branch_id: mã chi nhánh
     * - user_id: mã nhân viên
     */
    public static function getFileToClient($params = [])
    {
        $validate = \Validator::make($params,[
            'type'=>[
                'required',
                Rule::in(['image','file'])
            ],
            'option'=>[
                'required',
                Rule::in(['avatars','documents','others'])
            ]
        ]);
                
        if($validate->fails()){
            return trans('core.invalid_format');
        }

        $type = $params['type'];
        $option = $params['option'];
        $org_id = $params['org_id'];
        $branch_id = $params['branch_id'];
        $user_id = $params['user_id'];

        $uploaded = Upload::where([
            'org_id'=>$org_id,
            'branch_id'=>$branch_id,
            'type'=>$type,
            'user_id'=>$user_id,
        ])->get();
        
        if(!empty($uploaded)){
            foreach($uploaded as $item){
                if(strpos($item->path,$option)){
                    if($option == 'avatars'){
                        $name = substr(strrchr($item->path,$option."/"),4);
                    }elseif($option == 'documents'){
                        $name = substr(strrchr($item->path,$option."/"),10);
                    }else{//others
                        $name = substr(strrchr($item->path,$option."/"),7);
                    }
                    
                    //create request
                    $testDomain = 'http://localhost:8000/api/';
                    $realDomain = 'http://kkh-rest-api.herokuapp.com/api'; 

                    $data = [
                        'test-domain'=>$testDomain,
                        'real-domain'=>$realDomain,
                        'route'=>'get-image?name='.$name,
                        'type'=>$item->type,
                        'extension'=>$item->extension,
                        'full-path'=>$item->path
                    ];
                    return $data;
                }
            }
        }
        return [];
    }

    /**
     * Insert vào collection Upload và lưu vào folder public
     * params truyền vào:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - org_id: mã tổ chức
     * - branch_id: mã chi nhánh
     * - user_id: mã nhân viên
     */
    public static function uploadToServer($params = [], Request $request){
        $org_id = $params['org_id'];
        $branch_id = $params['branch_id'];
        $user_id = $params['user_id'];
        //$extension = $params['extension'];
        
        //Kiểm tra type và option
        $validate = \Validator::make($params,[
            'type'=>[
                'required',
                Rule::in(['image','file'])
            ],
            'option'=>[
                'required',
                Rule::in(['avatars','documents','others'])
            ]
        ]);
                
        if($validate->fails()){
            return trans('core.invalid_format');
        }
        
        $type = $params['type'];
        $option = $params['option'];
        //Kết thúc kiểm tra type và option

        /**
         * Lưu file vào path public/...
         */
        $files= $request->input('file');
        $file_name = $request->file('file')->getClientOriginalName();
        
        if($type=='image'){
            if($option == 'avatars'){
                $destination = base_path() . '/public/img/avatars';
                $realPath =  '/public/img/avatars/'.$file_name;
            }else{
                $destination = base_path() . '/public/img/others';
                $realPath =  '/public/img/others/'.$file_name;
            }
            
        }else{
            if($option == 'documents'){
                $destination = base_path() . '/public/files/documents';
                $realPath =  '/public/files/documents/'.$file_name;
            }else{
                $destination = base_path() . '/public/files/others';
                $realPath =  '/public/files/others/'.$file_name;
            }
        }
        
        $temp = $request->file('file')->move($destination,$file_name);
        /**
         * Kết thúc lưu file vào path public/...
         */

        /**
         * lưu file vào database
         */
        //Xử lý extension
        $file_extension = strtolower(substr(strrchr($file_name,"."),1));
        
        $uploadParams = [
            'path'=>$realPath,
            'org_id'=>$org_id,
            'branch_id'=>$branch_id,
            'user_id'=>$user_id,
            'type'=>$type,
            'extension'=>$file_extension
        ];

        $createUpload = Upload::create($uploadParams);
        return trans('core.success');
    }
}