<?php
namespace App\Libraries\Gma\APIs;

use App\Api\Entities\EmployeeTimeKeeper;
use App\Api\Entities\EmployeeRegisterShift;

class APIEmployee {

    /*
    *API chấm công
    **/
    public static function checkInOut($params = []){
        $timeInput = $params['time'];
        $type = $params['type'];
        $shiftId = $params['shift_id'];
        
        $checkShiftId = EmployeeTimeKeeper::where([
            '_id' => mongo_id($shiftId)
        ])->first();

        //nếu như công đã được tạo thì cho phép chấm công
        if(!empty($checkShiftId)){
            if($type == 'check-in'){
                if(isset($checkShiftId->check_in)){
                    return trans('employeeTimeKeeper.can-not-check-in');
                }
                $checkShiftId->check_in = $timeInput;
            }else{
                //Nếu là chấm ra ca, thì kiểm tra xem đã chấm vào ca chưa nếu chưa thì set check_in check_out bằng nhau
                if(isset($checkShiftId->check_in)){
                    if(isset($checkShiftId->check_out)){
                        return trans('employeeTimeKeeper.can-not-check-out');
                    }
                    $checkShiftId->check_out = $timeInput;
                }else{
                    $checkShiftId->check_in = $timeInput;
                    $checkShiftId->check_out = $timeInput;
                }

                //tính ra tổng thời gian làm được trong ca
                $sub_time = (strtotime($checkShiftId->check_out) - strtotime($checkShiftId->check_in)) / 3600;
                $checkShiftId->sub_time = $sub_time;
                $checkShiftId->status = 1;
            }
        }
        $checkShiftId->save();
        return trans('employeeTimeKeeper.success');
    }

    /*
    *API Tạo công
    **/
    public static function enrollShift($params = []){
        $shift_id = $params['shift_id'];
        $branch_id = $params['branch_id'];
        $org_id = $params['org_id'];
        $emp_id = $params['emp_id'];
        $date = $params['date'];

        $attrs = [
            'shift_id' => $shift_id,
            'branch_id' => $branch_id,
            'org_id' => $org_id,
            'emp_id' => $emp_id,
            'date' => $date,
            'status' => -1
        ];

        //Validate
        $validator = \Validator::make($attrs,[
            'shift_id' => 'required',
            'branch_id' => 'required',
            'org_id' => 'required',
            'emp_id' => 'required',
            'date' => 'required'
        ]);

        if($validator->fails()){
            return $validator->messages()->toArray();
        }

        $enrollShift = EmployeeTimeKeeper::create($attrs);
        return trans('core.success');
    }
}
