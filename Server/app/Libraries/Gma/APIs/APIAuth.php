<?php
namespace App\Libraries\Gma\APIs;

use App\Api\Entities\Branch;
use App\Api\Entities\Organizations;
use App\Api\Entities\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class APIAuth {
    /**
     * API tạo tài khoản người dùng
     */
    public static function createUserAccount($params = []){
        $user_id = $params['user_id'];
        $password = $params['password'];
        $branch_id = $params['branch_id'];
        $org_id = $params['org_id'];

        //Kiểm tra thông tin tổ chức
        $org_info = Organizations::where([
            '_id' => mongo_id($org_id)
        ])->first();
        if(empty($org_info)){
            return trans('organizations.no_org');
        }
        /**
         * Xử lý user name
         * Input: tanca gma
         * Output: tancagma
         */
        $user = remove_sign(build_user_name($org_info->org_name));

        /**
         * Mã hoá mật khẩu
         */
        $password = password_hash($password,PASSWORD_BCRYPT);

        //Kiểm tra thông tin của chi nhánh
        $branch_info = Branch::where([
            '_id'=>mongo_id($branch_id)
        ])->first();
        if(empty($branch_info)){
            return trans('branch.no-branch');
        }
        
        $attribute = [
            'user_id'=>$user_id,
            'user_name'=>$user,
            'password'=>$password,
            'branch_id'=>$branch_id,
            'org_id'=>$branch_id,
        ];

        $userAccount = Users::create($attribute);
        return trans('core.success');
    }

    /**
     * Xoá tài khoản người dùng
     */
    public static function deleteAccount($user_id = ''){
        if(!empty($user_id)){
            $account_info = Users::where([
                'user_id'=>mongo_id($user_id)
            ])->first();
            
            if(!empty($account_info)){
                $account_info->deleted_user = Auth::getPayLoad('user_id');
                $account_info->save();
                $account_info->delete();

                return trans('core.success');
            }
        }
        return trans('employee.no_employee');
    }
}


