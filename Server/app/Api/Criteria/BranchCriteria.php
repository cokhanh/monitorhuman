<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class BranchCriteria
 */
class BranchCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['id'])){
            $query->where('_id',mongo_id($this->params['id']));
        }
        if(!empty($this->params['no_sign'])){
            $no_sign = '%'.(string)($this->params['no_sign']).'%';
            $query->where('no_sign','like',$no_sign);
        }
        if(!empty($this->params['org_id'])){
            $query->where('org_id',$this->params['org_id']);
        }
        if(!empty($this->params['phone'])){
            $phone = '%'.(string)($this->params['phone']).'%';
            $query->where('phone','like',$phone);
        }
        return $query;
    }
}
