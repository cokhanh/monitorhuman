<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class EmployeeRegisterShiftCriteria
 */
class EmployeeRegisterShiftCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['org_id'])){
            $query->where('org_id',$this->params['org_id']);
        }
        if(!empty($this->params['branch_id'])){
            $query->where('branch_id',$this->params['branch_id']);
        }
        if(!empty($this->params['_id'])){
            $query->where('_id',mongo_id($this->params['_id']));
        }
        if(!empty($this->params['emp_id'])){
            $query->where('emp_id',$this->params['emp_id']);
        }
        
        return $query;
    }
}
