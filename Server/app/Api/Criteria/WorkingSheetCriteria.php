<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class WorkingSheetCriteria
 */
class WorkingSheetCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['id'])){
            $query->where('_id',mongo_id($this->params['id']));
        }
        if(!empty($this->params['org'])){
            $query->where('org_id',$this->params['org']);
        }
        if(!empty($this->params['check_in'])){
            //$check_in = '%'.(string)($this->params['check_in']).'%';
            if(!empty($this->params['check_out'])){
                //$check_out = '%'.(string)($this->params['check_out']).'%';
                $query->where('check_in','>=',$this->params['check_in'])
                    ->where('check_out','<=',$this->params['check_out']);
            }else{
                $query->where('check_in','>=',$this->params['check_in']);
            }
        }
        return $query;
    }
}
