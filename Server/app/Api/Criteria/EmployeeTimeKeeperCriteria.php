<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class EmployeeTimeKeeperCriteria
 */
class EmployeeTimeKeeperCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();
        //djson($this->params['status']);
        if(!empty($this->params['branch_id'])){
            $query->where('branch_id',$this->params['branch_id']);
        }
        if(!empty($this->params['emp_id'])){
            $query->where('emp_id',$this->params['emp_id']);
        }
        if(!empty($this->params['status'])){
            $query->where('status',(int)$this->params['status']);
        }
        if(!empty($this->params['from_date'])){
            if(!empty($this->params['to_date'])){
                $query->where('date','>=',$this->params['from_date'])
                    ->where('date','<=',$this->params['to_date']);
            }
            else{
                $query->where('date','>=',$this->params['from_date']);
            }
        }
        if(!empty($this->params['_id'])){
            $query->where('_id',mongo_id($this->params['_id']));
        }
        return $query;
    }
}
