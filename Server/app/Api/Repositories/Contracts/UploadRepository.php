<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UploadRepository
 */
interface UploadRepository extends RepositoryInterface
{
    public function getUploadedFiles($params = [],$limit = 0);
}
