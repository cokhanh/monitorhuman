<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmployeeTimeKeeperRepository
 */
interface EmployeeTimeKeeperRepository extends RepositoryInterface
{
    public function getTimeKeeper($params = [], $limit = 0);
}
