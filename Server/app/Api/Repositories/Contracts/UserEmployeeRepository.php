<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEmployeeRepository
 */
interface UserEmployeeRepository extends RepositoryInterface
{
    public function getEmployee($params = [],$limit = 0);
}
