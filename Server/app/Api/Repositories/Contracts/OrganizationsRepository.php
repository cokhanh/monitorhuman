<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrganizationsRepository
 */
interface OrganizationsRepository extends RepositoryInterface
{
    
}
