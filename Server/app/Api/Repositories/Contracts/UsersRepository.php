<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersRepository
 */
interface UsersRepository extends RepositoryInterface
{
    public function getUsersList($params = [],$limit = 0);
}
