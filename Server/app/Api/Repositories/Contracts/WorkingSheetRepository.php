<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WorkingSheetRepository
 */
interface WorkingSheetRepository extends RepositoryInterface
{
    public function getWorkingSheetShift($params = [],$limit = 0);
}
