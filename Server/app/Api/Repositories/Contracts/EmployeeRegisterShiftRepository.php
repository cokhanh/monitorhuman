<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmployeeRegisterShiftRepository
 */
interface EmployeeRegisterShiftRepository extends RepositoryInterface
{
    public function getRegisterShift($params = [], $limit = 0);
}
