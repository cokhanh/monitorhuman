<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\OrganizationsRepository;
use App\Api\Entities\Organizations;
use App\Api\Validators\OrganizationsValidator;

/**
 * Class OrganizationsRepositoryEloquent
 */
class OrganizationsRepositoryEloquent extends BaseRepository implements OrganizationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Organizations::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
