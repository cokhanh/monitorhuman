<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\EmployeeRegisterShiftCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\employeeRegisterShiftRepository;
use App\Api\Entities\EmployeeRegisterShift;
use App\Api\Validators\EmployeeRegisterShiftValidator;

/**
 * Class EmployeeRegisterShiftRepositoryEloquent
 */
class EmployeeRegisterShiftRepositoryEloquent extends BaseRepository implements EmployeeRegisterShiftRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeeRegisterShift::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getRegisterShift($params = [], $limit = 0){
        $this->pushCriteria(new EmployeeRegisterShiftCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit == 0){
                $item = $this->paginate();
            }else{
                $item = $this->paginate($limit);
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new EmployeeRegisterShiftCriteria($params));
        return $item;
    }
}
