<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UserEmployeeCriteria;
use App\Api\Criteria\WorkingSheetCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\workingSheetRepository;
use App\Api\Entities\WorkingSheet;
use App\Api\Validators\WorkingSheetValidator;

/**
 * Class WorkingSheetRepositoryEloquent
 */
class WorkingSheetRepositoryEloquent extends BaseRepository implements WorkingSheetRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WorkingSheet::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
    public function getWorkingSheetShift($params = [],$limit = 0){
        $this->pushCriteria(new WorkingSheetCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit != 0){
                $item = $this->paginate($limit);
            }else{
                $item = $this->paginate();
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new WorkingSheetCriteria($params));
        return $item;
    }
}
