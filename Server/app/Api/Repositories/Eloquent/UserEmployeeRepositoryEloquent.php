<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UserEmployeeCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\UserEmployeeRepository;
use App\Api\Entities\UserEmployee;
use App\Api\Validators\UserEmployeeValidator;

/**
 * Class UserEmployeeRepositoryEloquent
 */
class UserEmployeeRepositoryEloquent extends BaseRepository implements UserEmployeeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserEmployee::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getEmployee($params = [],$limit = 0){
        $this->pushCriteria(new UserEmployeeCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit != 0){
                $item = $this->paginate($limit);
            }else{
                $item = $this->paginate();
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new UserEmployeeCriteria($params));
        return $item;
    }
}
