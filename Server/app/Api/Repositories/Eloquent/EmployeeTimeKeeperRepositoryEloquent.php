<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\EmployeeTimeKeeperCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\employeeTimeKeeperRepository;
use App\Api\Entities\EmployeeTimeKeeper;
use App\Api\Validators\EmployeeTimeKeeperValidator;

/**
 * Class EmployeeTimeKeeperRepositoryEloquent
 */
class EmployeeTimeKeeperRepositoryEloquent extends BaseRepository implements EmployeeTimeKeeperRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeeTimeKeeper::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getTimeKeeper($params = [], $limit = 0){
        $this->pushCriteria(new EmployeeTimeKeeperCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit == 0){
                $item = $this->paginate();
            }else{
                $item = $this->paginate($limit);
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new EmployeeTimeKeeperCriteria($params));
        return $item;
    }
}
