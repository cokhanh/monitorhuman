<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\OrganizationsTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Organizations extends Moloquent
{
	use SoftDeletes;

	protected $collection = '';

    protected $fillable = ['org_name','phone','email','deleted_user'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new OrganizationsTransformer();

        return $transformer->transform($this);
    }

}
