<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\BranchTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Branch extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Branch';

    protected $fillable = ['branch_name','org_id','phone','address','email','no_sign','deleted_user'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = null)
    {
        $transformer = new BranchTransformer();

        return $transformer->transform($this,$type);
    }

}
