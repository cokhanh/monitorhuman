<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UploadTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Upload extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'upload';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '')
    {
        $transformer = new UploadTransformer();

        return $transformer->transform($this,$type);
    }

}
