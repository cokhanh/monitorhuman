<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\EmployeeRegisterShiftTransformer;
use Moloquent\Eloquent\SoftDeletes;

class EmployeeRegisterShift extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'employeeRegisterShift';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '')
    {
        $transformer = new EmployeeRegisterShiftTransformer();

        return $transformer->transform($this,$type);
    }

}
