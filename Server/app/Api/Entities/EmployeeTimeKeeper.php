<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\EmployeeTimeKeeperTransformer;
use Moloquent\Eloquent\SoftDeletes;

class EmployeeTimeKeeper extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'empployeeTimeKeeper';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '')
    {
        $transformer = new EmployeeTimeKeeperTransformer();

        return $transformer->transform($this,$type);
    }

}
