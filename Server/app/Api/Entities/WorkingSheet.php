<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\WorkingSheetTransformer;
use Moloquent\Eloquent\SoftDeletes;

class WorkingSheet extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'WorkingSheetShift';

    protected $fillable = ['shift_name','check_in','check_out','org_id','no_sign','deleted_user'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = null)
    {
        $transformer = new WorkingSheetTransformer();

        return $transformer->transform($this,$type);
    }

}
