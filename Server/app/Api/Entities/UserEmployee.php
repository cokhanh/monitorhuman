<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UserEmployeeTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UserEmployee extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'UserEmployee';

    protected $fillable = ['full_name','birthday','phone','email','deleted_user','org_id','branch_id','no_sign','sex','role',
        'role_no_sign'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    //System role
    const MANAGER = 'Manager';
    const MANAGER_TRIAL = 'Manager\'s Trial Version';

    const BRANCH_MANAGER = 'Branch\'s Manager';
    const BRANCH_MANAGER_TRIAL = 'Branch Manager\'s Trial Version';
    
    const STAFF = 'Staff';
    const STAFF_TRIAL = 'Staff\'s Trial Version';

    const SYSTEM_ADMIN = 'System\'s Admin';

    public function transform(string $type = null)
    {
        $transformer = new UserEmployeeTransformer();

        return $transformer->transform($this,$type);
    }

}
