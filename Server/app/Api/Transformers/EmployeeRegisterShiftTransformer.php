<?php

namespace App\Api\Transformers;

use App\Api\Entities\Branch;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\EmployeeRegisterShift;
use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use App\Api\Entities\WorkingSheet;

/**
 * Class EmployeeRegisterShiftTransformer
 */
class EmployeeRegisterShiftTransformer extends TransformerAbstract
{

    /**
     * Transform the \EmployeeRegisterShift entity
     * @param \EmployeeRegisterShift $model
     *
     * @return array
     */
    public function transform(EmployeeRegisterShift $model)
    {
        $data = [
            '_id' => $model->_id,
            'date' => $model->date
        ];

        //Kiểm tra thông tin nhân viên
        if(isset($model->emp_id)){
            $emp_info = UserEmployee::where([
                '_id' => mongo_id($model->emp_id)
            ])->first();
            if(!empty($emp_info)){
                $data['emp_name'] = $emp_info->full_name;
            }
        }

        //Kiểm tra thông tin tổ chức
        if(isset($model->org_id)){
            $org_info = Organizations::where([
                '_id' => mongo_id($model->org_id)
            ])->first();
            if(!empty($org_info)){
                $data['org_name'] = $org_info->org_name;
            }
        }

        //Kiểm tra thông tin ca làm được đăng kí
        if(isset($model->shift_selected)){
            $shift_info = WorkingSheet::where([
                '_id' => mongo_id($model->shift_selected)
            ])->first();
            if(!empty($shift_info)){
                $data['shift'] = $shift_info->shift_name;
            }
        }

        //Kiểm tra thông tin chi nhánh
        if(isset($model->branch_id)){
            $branch_info = Branch::where([
                '_id' => mongo_id($model->branch_id)
            ])->first();
            if(!empty($branch_info)){
                $data['branch_name'] = $branch_info->branch_name;
            }
        }

        if(isset($model->status)){
            $data['status'] = trans('employee.confirmed');
        }

        return $data;
    }
}
