<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\UserToken;

/**
 * Class UserTokenTransformer
 */
class UserTokenTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserToken entity
     * @param \UserToken $model
     *
     * @return array
     */
    public function transform(UserToken $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
