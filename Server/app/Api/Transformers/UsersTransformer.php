<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Users;

/**
 * Class UsersTransformer
 */
class UsersTransformer extends TransformerAbstract
{

    /**
     * Transform the \Users entity
     * @param \Users $model
     *
     * @return array
     */
    public function transform(Users $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
