<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Upload;

/**
 * Class UploadTransformer
 */
class UploadTransformer extends TransformerAbstract
{

    /**
     * Transform the \Upload entity
     * @param \Upload $model
     *
     * @return array
     */
    public function transform(Upload $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
