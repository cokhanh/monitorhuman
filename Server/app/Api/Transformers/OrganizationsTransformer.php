<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Organizations;

/**
 * Class OrganizationsTransformer
 */
class OrganizationsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Organizations entity
     * @param \Organizations $model
     *
     * @return array
     */
    public function transform(Organizations $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
