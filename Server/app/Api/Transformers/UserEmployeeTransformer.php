<?php

namespace App\Api\Transformers;

use App\Api\Entities\Organizations;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\UserEmployee;

/**
 * Class UserEmployeeTransformer
 */
class UserEmployeeTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserEmployee entity
     * @param \UserEmployee $model
     *
     * @return array
     */
    public function transform(UserEmployee $model)
    {

        $data = [
            'id'=>$model->_id,
            'full_name'=>$model->full_name,
            'birthday'=>$model->birthday,
            'phone'=>$model->phone,
            'email'=>$model->email,
            'role'=>$model->role
        ];

        $org = Organizations::where(['_id'=>mongo_id($model->org_id)])->first();
        //$branch = Branch::where(['_id',mongo_id($model->branch_id)])->first();
        if(!empty($org)){
            $data['organization'] = $org->org_name;
        }
        if((int)$model->sex == 1){
            $data['sex'] = trans('employee.male');
        }else{
            $data['sex'] = trans('employee.female');
        }

        return $data;
    }
}
