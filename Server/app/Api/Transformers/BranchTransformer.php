<?php

namespace App\Api\Transformers;

use App\Api\Entities\Organizations;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\Branch;

/**
 * Class BranchTransformer
 */
class BranchTransformer extends TransformerAbstract
{

    /**
     * Transform the \Branch entity
     * @param \Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        $data = [
            'id'=>$model->_id,
            'branch_name'=>$model->branch_name,
            'address'=>$model->address,
            'phone'=>$model->phone,
            'email'=>$model->email,
        ];

        $org = Organizations::where(['_id'=>mongo_id($model->org_id)])->first();
        if(!empty($org)){
            $data['org'] = $org->org_name;
        }

        return $data;
    }
}
