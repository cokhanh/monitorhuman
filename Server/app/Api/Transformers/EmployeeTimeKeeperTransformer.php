<?php

namespace App\Api\Transformers;

use App\Api\Entities\Branch;
use App\Api\Entities\EmployeeRegisterShift;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\EmployeeTimeKeeper;
use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use App\Api\Entities\WorkingSheet;

/**
 * Class EmployeeTimeKeeperTransformer
 */
class EmployeeTimeKeeperTransformer extends TransformerAbstract
{

    /**
     * Transform the \EmployeeTimeKeeper entity
     * @param \EmployeeTimeKeeper $model
     *
     * @return array
     */
    public function transform(EmployeeTimeKeeper $model, string $type = '')
    {
        $data = [
            '_id' => $model->_id,
            'date' => $model->date,
        ];

        //Kiểm tra thông tin ca làm
        if(isset($model->shift_id)){
            $shift_detail_info = EmployeeRegisterShift::where([
                '_id'=>mongo_id($model->shift_id)
            ])->first();
            if(!empty($shift_detail_info)){
                $shift_detail_name = WorkingSheet::where([
                    '_id' => mongo_id($shift_detail_info->shift_selected) 
                ])->first();
                if(!empty($shift_detail_name)){
                    $data['shift'] = $shift_detail_name->shift_name;
                }
            }
        }

        //Kiểm tra thông tin chi nhánh
        if(isset($model->branch_id)){
            $branch_info = Branch::where([
                '_id'=>mongo_id($model->branch_id)
            ])->first();
            if(!empty($branch_info)){
                $data['branch'] = $branch_info->branch_name;
            }
        }

        //Kiểm tra thông tin tổ chức
        if(isset($model->org_id)){
            $org_info = Organizations::where([
                '_id' => mongo_id($model->org_id)
            ])->first();
            if(!empty($org_info)){
                $data['org'] = $org_info->org_name;
            }
        }

        //Kiểm tra thông tin nhân viên
        if(isset($model->emp_id)){
            $emp_info = UserEmployee::where([
                '_id' => mongo_id($model->emp_id)
            ])->first();
            if(!empty($emp_info)){
                $data['emp'] = $emp_info->full_name;
            }
        }

        if(isset($model->sub_time)){
            $data['subtime'] = $model->sub_time;
        }else{
            $data['sub_time'] = 0;
        }

        if($model->status == -1){
            $data['status'] = trans('employee.not-confirmed');
        }else{
            $data['status'] = trans('employee.confirmed');
        }

        if($type == 'get-notes'){
            $data['note'] = $model->note;
        }
        return $data;
    }
}
