<?php

namespace App\Api\Transformers;

use App\Api\Entities\Organizations;
use App\Api\Entities\UserEmployee;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\WorkingSheet;

/**
 * Class WorkingSheetTransformer
 */
class WorkingSheetTransformer extends TransformerAbstract
{

    /**
     * Transform the \WorkingSheet entity
     * @param \WorkingSheet $model
     *
     * @return array
     */
    public function transform(WorkingSheet $model,string $type = null)
    {
        $data = [
            'id'=>$model->_id,
            'shift_name'=>$model->shift_name,
            'check_in'=>$model->check_in,
            'check_out'=>$model->check_out
        ];

        $user = UserEmployee::where(['_id'=>mongo_id($model->deleted_user)])->first();
        if(!empty($user)){
            $data['deleted_user'] = $user->full_name;
        }

        $org = Organizations::where(['_id'=>mongo_id($model->org_id)])->first();
        if(!empty($org)){
            $data['org'] = $org->org_name;
        }

        if($type == 'included-by-news'){
            return $data['shift_name'];
        }

        return $data;
    }
}
