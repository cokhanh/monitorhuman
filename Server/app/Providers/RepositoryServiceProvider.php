<?php

namespace app\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $models = array(
            'Test',
            'Organizations',
            'UserEmployee',
            'Branch',
            'WorkingSheet',
            'EmployeeTimeKeeper',
            'EmployeeRegisterShift',
            'Users',
            'UserToken',
            'Wifi',
            'Upload',
        );

        foreach ($models as $model) {
            $this->app->bind("App\\Api\\Repositories\\Contracts\\{$model}Repository", "App\\Api\\Repositories\\Eloquent\\{$model}RepositoryEloquent");
        }
    }
}
