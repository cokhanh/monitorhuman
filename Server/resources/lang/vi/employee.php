<?php
return [
    'no_employee'=>'Nhân viên không tồn tại',
    'male'=>'Nam',
    'female'=>'Nữ',
    'is-not-staff-of-org'=>'Bạn không phải thành viên của tổ chức này',
    'no_registered_shift'=>'Không có ca làm được đăng kí',
    'confirmed' => 'Đã duyệt',
    'not-confirmed' => 'Chưa được duyệt'
];