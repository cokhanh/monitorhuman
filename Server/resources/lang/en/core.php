<?php

return [
    'success' => 'Success',
    'norecord'=> 'No data',
    'invalid_month' => 'Invalid month',
    'invalid_format'=>'Định dạng không hợp lệ',
    'no_path'=>'No such directory in application'
];
