<?php
return [
    'no-branch'=>'Branch does not exist',
    'unavailable-branch'=>'Branch is unavailable'
];